if status is-interactive
    # Commands to run in interactive sessions can go here
end

set -x EDITOR /usr/bin/nvim
#set -x DOCKER_HOST unix://$XDG_RUNTIME_DIR/podman/podman.sock
set -x JAVA_HOME $(dirname $(dirname $(readlink $(readlink $(which javac)))))


kubectl completion fish | source

