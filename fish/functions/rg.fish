function rg --description 'alias rg=rg --no-ignore-vcs --smart-case'
 command rg --no-ignore-vcs --smart-case $argv; 
end
